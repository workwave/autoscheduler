# param (
    
#     [ValidateSet('Unit' , 'Compliance')]
#     $TestType
# )

# switch($TestType){
#     'Unit' {
#         $Container = New-PesterContainer -Path '.\tests\unit\' 
#     }

#     'Compliance' {
#         $Container = New-PesterContainer -Path '.\tests\compliance\' 
#     }
# }

# $PesterConfiguration = New-PesterConfiguration
# $PesterConfiguration.CodeCoverage.Enabled = $false
# $PesterConfiguration.CodeCoverage.OutputFormat = 'JaCoCo'
# $PesterConfiguration.TestResult.Enabled = $true
# $PesterConfiguration.TestResult.OutputFormat = 'NUnitXml'
# $PesterConfiguration.Run.PassThru = $true
# $PesterConfiguration.Run.Container = $Container
# $PesterConfiguration.Output.Verbosity = "Detailed"

# Invoke-Pester -Configuration $PesterConfiguration 
# C:\ReportUnit.1.2.1\tools\ReportUnit.exe "testResults.xml"
checkov --directory .\infrastructure -o junitxml --output-file-path .