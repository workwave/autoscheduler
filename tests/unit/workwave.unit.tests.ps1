BeforeAll -Scriptblock {
    Push-Location -Path '.\infrastructure'
    terraform init
    tflint --init
    $script:tfvalidate = (terraform validate -json | ConvertFrom-Json)
    $script:tflint = (tflint --format json | ConvertFrom-Json)
    $script:tfsec = (tfsec --format json | ConvertFrom-Json)
}

Describe -Name "workwave" -Tag "unit" -Fixture {
    Context -Name "folder existance" -Tag "unit.test.folder" -Fixture {
        It -Name "modules folder should exists" {
            Test-Path -Path ..\modules | Should -Be $true
        }

        It -Name "src folder should exists" {
            Test-Path -Path ..\src | Should -Be $true
        }
    }

    Context -Name "terraform validate" -Tag "unit.terraform.validate" -Fixture {
        It -Name "terrafrom validate should be true" {
            $script:tfvalidate.valid | Should -Be $true
        }

        It -Name "terraform validate severity should not be error" {
            $tfvalidate.diagnostics.foreach({ $_.severity | Should -Not -Be 'error' })
        }
    }
    
    Context -Name "terraform lint" -Tag "unit.tflint" -Fixture {
        It -Name "terraform linting errors should be null" {
            $tflint.errors | Should -Be $null
        }

        It -Name "terraform linting serverity should not have error" {
            $tflint.issues.rule.ForEach({ $_.severity | Should -BeExactly 'warning' })
        } 
    }

    Context -Name "terraform static code analysis" -Tag "unit.tfsec" -Fixture {
        It -Name "terrafrom security results count should be 0" {
            $tfsec.results.Count | Should -BeExactly 0
        }

        It -Name "terraform security should not have CRITICAL bugs" {
            $tfsec.results.ForEach({ $_.severity | Should -Not -Be 'CRITICAL' })
        }
    }
}

AfterAll -Scriptblock {
    Pop-Location
}