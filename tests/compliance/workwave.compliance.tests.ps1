BeforeAll -Scriptblock {
    Push-Location .\infrastructure
    terraform init
    terraform plan -out='autoscheduler.plan'
    $script:plan = (terraform show -json 'autoscheduler.plan' | ConvertFrom-Json)
}
Describe -Name "compliance" -Fixture {
    Context -Name "compliance.tests" -Fixture {
        It -Name "azurerm_storage_account min TLS version should be" -Test {
            $plan.resource_changes.Where({ $_.type -eq 'azurerm_storage_account' }).change.after.min_tls_version | Should -BeExactly 'TLS1_2'  
        }

        It -Name "azurerm_storage_account account replication type should be LRS" -Test {
            $plan.resource_changes.Where({ $_.type -eq 'azurerm_storage_account' }).change.after.account_replication_type | Should -BeExactly 'LRS' 
        }
    }
}

AfterAll -Scriptblock {
    Pop-Location
}