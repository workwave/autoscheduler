variable "resource_group_name" {
  type    = string
  default = "rgp-test-dev"
}

variable "resource_group_location" {
  type    = string
  default = "eastus2"
}
